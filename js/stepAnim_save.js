let YEARS = [2005,2010,2015];
let PERSON_PER_BOID = 10000;
let ANIM_SPREAD_TIME = 5 * 1000; // 5 seconds

// get screen infos
let w = window,	d = document, e = d.documentElement, g = d.getElementsByTagName('body')[0],
	width = w.innerWidth||e.clientWidth||g.clientWidth,
	height = w.innerHeight||e.clientHeight||g.clientHeight,
	isMobile = width < 768,
	isVertical = width > height,
	ratioScreen = width / height,
	mapWidth = 1440,
	mapHeight = 900,
	ratioMap = mapWidth/mapHeight,
	svgHeight = (isMobile ? width :  height),
	svgWidth = (isMobile ? width :  height*ratioMap);

if(ratioScreen < ratioMap && !isMobile){
	svgWidth = width;
	svgHeight = svgWidth/ratioMap;
	document.querySelector(".mapContainer").classList.add("horizontal");
}


let svg = d3.select("svg")
		.attr("width", svgWidth)
		.attr("height", svgHeight);

if (isMobile) {
	svg.attr("viewBox", null);
} else {
	
}

let	qatar = svg.select("#qatar"),
	gCircle = svg.append("g"),
	date = d3.select(".date"),
	legendQatar = document.getElementById("legendQatar"),
	legendMigrant = document.getElementById("legendMigrant"),
	populationAmountQatarText = d3.select(".qatar .populationAmount"),
	populationAmountMigrantText = d3.select(".migrant .populationAmount"),
	positionOfQatarOnScreen = {
		x: parseInt(isMobile ? svgWidth/2 : qatar.select(".countryMark").attr("x")),
		y: parseInt(isMobile ? svgWidth/2 : qatar.select(".countryMark").attr("y"))
	},
	counterAnimQatar,
	counterAnimMigrant,
	interval = [],
	nodes = [],
	node,
	countries = Object.values(data),
	simulation = d3.forceSimulation(nodes)
    	.force("charge", d3.forceManyBody().strength(-0.3))
    	.force("x", d3.forceX(positionOfQatarOnScreen.x).strength(0.05))
    	.force("y", d3.forceY(positionOfQatarOnScreen.y).strength(0.05))
    	.alphaDecay(0);


function launchAnim(stepNum, dir) {

	simulation.on("tick", ticked);

	switch (stepNum){
		case 0 :
			initYear(stepNum, dir);
			break;
		case 1 :
			initYear(stepNum, dir);
			break;
		case 2 :
			initYear(stepNum, dir);
			break;
		default:
			console.log("stepNum");
	}
}

function init() {
	for (let i = 0; i < countries.length; i++) {
		let country = countries[i];
		let name = country.name;
		let isQatar = name == "Qatar";
		let countryElement = svg.select("#" + country.id);

		//init pos of countries
		if(isMobile){
			country.posX = (isQatar ? width/2 : 0);
			country.posY = (isQatar ? width/2 : 0);
		}else {
			country.posX = parseInt(countryElement.select(".countryMark").attr("x"));
			country.posY = parseInt(countryElement.select(".countryMark").attr("y"));
		}

		country.displayedBoids = 0;

		// add circle indicator on the map
		if(!isQatar){
			country.indicator = svg.append("circle")
					.attr("cx", country.posX)
					.attr("cy", country.posY)
					.attr("r", 0)
					.attr("class", (isQatar ? "indicator hidden" : "indicator"))
					.style("fill", (isQatar ? country.color : "#87CEFA"));
		}
	}
}



function initYear(step, dir) {

	let yearQatarAmount = data.qatar.presence[step];
	let yearMigrantAmount = countries.reduce( ( sum, { presence } ) => sum + presence[step], 0) - yearQatarAmount;


	if(step == 0 || dir == "up"){

		if(counterAnimQatar){
			counterAnimQatar.reset();
		}
		legendQatar.textContent = yearQatarAmount;
		console.log(yearQatarAmount);

		if(counterAnimMigrant){
			counterAnimMigrant.reset();
		}
		legendMigrant.textContent = yearMigrantAmount;

	} else {
		counterAnimQatar = new CountUp(
			ANIM_SPREAD_TIME, 
			yearQatarAmount, 
			legendQatar
		);
		counterAnimQatar.start();
		
		counterAnimMigrant = new CountUp(
			ANIM_SPREAD_TIME, 
			yearMigrantAmount, 
			legendMigrant
		);
		counterAnimMigrant.start();
	}


	for (let i = 0; i < countries.length; i++) {
		let country = countries[i];
		let name = country.name;
		let presence = country.presence[step];
		let boidsForThisStep = Math.round(presence / PERSON_PER_BOID);
		let diffBoids = boidsForThisStep - country.displayedBoids;
		let timeIntervalBetweenTwoBoids = Math.abs(ANIM_SPREAD_TIME / diffBoids);
		let posX = country.posX;
		let posY = country.posY;
		let indicator = country.indicator;

		clearInterval(interval[name]);
		date.text(YEARS[step]);

		if(diffBoids != 0){

			if(step == 0 || dir == "up"){
				updatePoint(country, positionOfQatarOnScreen, diffBoids);

			} else {
				interval[name] = setInterval(function(){
					updatePoint(country, {"x": posX, "y": posY}, (diffBoids < 0 ? -1 : 1) );
					// console.log(name, country.displayedBoids, boidsForThisStep);
					if(country.displayedBoids >= boidsForThisStep){
						console.log("clearInterval");
						clearInterval(interval[name]);
					}
				}, timeIntervalBetweenTwoBoids);
			}
		}
	}
}

function updatePoint (country, coord, diffBoids) {
	let name = country.name;
	let isQatar = name == "Qatar";
	if(diffBoids > 0){
		addPoint({
			fill: (isQatar ? country.color : "#87CEFA"),
			country: name,
			x: isQatar || !isMobile ? coord.x : 0,
			y: isQatar || !isMobile ? coord.y : 0
		},diffBoids);
	} else {
		removePoint(name, Math.abs(diffBoids));
	}
	country.displayedBoids = document.querySelectorAll("[data-country='"+name+"']").length;
	if(!isQatar)
		country.indicator.attr("r",2 * Math.sqrt(country.displayedBoids));
}

function ticked() {
	node.attr("cx", function(d) { return d.x; })
		.attr("cy", function(d) { return d.y; });
}

function restart() {

	node = gCircle.selectAll("circle")
		.data(nodes)
		.join("circle")
		.attr("r", 2)
		.attr("cx", function(d){return d.x;})
		.attr("cy", function(d){return d.y;})
		.attr("data-country", function(d){return d.country;})
		.style("fill", function(d) { return d.fill; });
}


function addPoint(features,nbr = 1) {

	let isQatar = features.country == "Qatar";

	if(nbr > 1){
		nodes = nodes.concat(d3.range(nbr).map(function(i) {
			return {
				fill: features.fill,
				country: features.country,
				x: (isQatar || !isMobile ? features.x + Math.random()*10-5 : randomPos().x), //random to avoid graphical bug
				y: (isQatar || !isMobile ? features.y + Math.random()*10-5 : randomPos().y)
			};
		}));
	} else {
		nodes.push({
			fill: features.fill,
			country: features.country,
			x: (isQatar || !isMobile ? features.x : randomPos().x),
			y: (isQatar || !isMobile ? features.y : randomPos().y)
		});
	}
	restart();
	simulation.nodes(nodes);
	console.log("addPoint " + (nbr || 1));
}


function removePoint(country,nbr = 1) {
	let removedAmount = 0;
	for (let i = nodes.length - 1; i >= 0; i--) {
		if(nodes[i].country == country){
			nodes.splice(i, 1);
			removedAmount++;
			if(removedAmount == nbr ){
				break;
			}
		}
	}
	restart();
	simulation.nodes(nodes);
	console.log("removePoint " + nbr);
}

function randomPos(){
	let isXfixed = Math.round(Math.random()*10)%2;
	let isYfixed = !isXfixed;

	let x = 0;
	let y = 0;

	if(isXfixed){
		x = Math.round(Math.random()*10)%2*width;
		y = Math.random()*10*width;
	} else {
		x = Math.random()*10*width;
		y = Math.round(Math.random()*10)%2*width;
	}

	return {"x":x, "y":y };
}



init();