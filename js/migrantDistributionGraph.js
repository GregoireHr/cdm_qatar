const dataChart = [
{name: "Indonesie", nb: 43059, "pc":1.9},
{name: "Sri lanka", nb: 54731, "pc":2.5},
{name: "Pakistan", nb: 133212, "pc":6},
{name: "Philippines", nb: 148496, "pc":6.6},
{name: "Nepal", nb: 151314, "pc":6.8},
{name: "Egypt", nb: 163569, "pc":7.3},
{name: "Bangladesh", nb: 180183, "pc":8.1},
{name: "Qatar", nb: 306948, "pc":13.7},
{name: "Inde", nb: 1052013, "pc":47.1}
];

const svgChart = d3.select(".migrantDistributionGraph").append("svg")
  .style('max-width', '100%')
  .style('height', 'auto');

const barChart = svgChart.append("g");
var margin = {top: 20, right: 20, bottom: 20, left: 20},
    widthGraph = 320 - margin.left - margin.right,
    heightGraph = 500 - margin.top - margin.bottom;


     

var scaleY = d3.scaleBand()
          .range([heightGraph, 0])
          .padding(0.1);

var scaleX = d3.scaleLinear()
          .range([0, widthGraph]);

svgChart.attr("width", widthGraph + margin.left + margin.right)
    .attr("height", heightGraph + margin.top + margin.bottom)
    .append("g")
    .attr("transform", 
          "translate(" + margin.left + "," + margin.top + ")");

  scaleX.domain([0, d3.max(dataChart, function(d){ return d.nb; })])
  scaleY.domain(dataChart.map(function(d) { return d.name; }));

  
  svgChart.selectAll(".bar")
      .data(dataChart)
    .enter().append("rect")
      .attr("class", "bar")
      .attr("width", function(d) {return scaleX(d.nb); } )
      .attr("y", function(d) { return scaleY(d.name); })
      .attr("fill", function(d) { return (d.name == "Qatar" ? "#e4a4a4" : "#c0e7ff" ); })
      .attr("height", scaleY.bandwidth());

  // add the x Axis
  svgChart.append("g")
      .attr("transform", "translate(0," + heightGraph + ")")
      .call(d3.axisBottom(scaleX)
        .tickFormat(d3.format("d"),"m")
        .tickValues([0, 1052013])
      );

  // add the y Axis
  svgChart.append("g").attr("class", "graphLeftScale")
      .call(d3.axisRight(scaleY)
        .tickSize(0)
        .tickPadding(8)
      );

      //ticksize Wwwwwww