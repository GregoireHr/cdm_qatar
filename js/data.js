var data = 
{
"india": {
	"presence":[
		/*"2005":*/ 193404,
		/*"2010":*/ 540914,
		/*"2015":*/ 1052013
	],
	"color": "#87CEFA",
	"name" : "India",
	"id" : "india"
},
"qatar" : {
	"presence":[
		/*"2005":*/ 192586,
		/*"2010":*/ 243073,
		/*"2015":*/ 306948
	],
	"color": "#8B0000",
	"name" : "Qatar",
	"id" : "qatar"
}
,
"nepal": {
	"presence":[
		/*"2005":*/ 23647,
		/*"2010":*/ 66011,
		/*"2015":*/ 151314
	],
	"color": "#90b7ff",
	"name" : "Nepal",
	"id" : "nepal"
},
"bangladesh": {
	"presence":[
		/*"2005":*/ 75689,
		/*"2010":*/ 169370,
		/*"2015":*/ 180183
	],
	"color": "#00CED1",
	"name" : "Bangladesh",
	"id" : "bangladesh"
},
"egypt": {
	"presence":[
		/*"2005":*/ 48490,
		/*"2010":*/ 136060,
		/*"2015":*/ 163569
	],
	"color": "#7878ca",
	"name" : "Egypt",
	"id" : "egypt"
},
"indonesia": {
	"presence":[
		/*"2005":*/ 64612,
		/*"2010":*/ 48072,
		/*"2015":*/ 43059
	],
	"color": "#a1dfec",
	"name" : "Indonesia",
	"id" : "indonesia"
},
"philippines": {
	"presence":[
		/*"2005":*/ 54268,
		/*"2010":*/ 107963,
		/*"2015":*/ 148496
	],
	"color": "#9797c7",
	"name" : "Philippines",
	"id" : "philippines"
},
"pakistan": {
	"presence":[
		/*"2005":*/ 51773,
		/*"2010":*/ 145204,
		/*"2015":*/ 133212
	],
	"color": "#9fd0de",
	"name" : "Pakistan",
	"id" : "pakistan"
},
"sri_lanka": {
	"presence":[
		/*"2005": */12231,
		/*"2010":*/ 33403,
		/*"2015":*/ 54731
	],
	"color": "#6495ED",
	"name" : "Sri Lanka",
	"id" : "sri_lanka"
}
};


