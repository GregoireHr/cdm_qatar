let YEARS = [2005,2010,2015];
let PERSON_PER_BOID = 10000;


// get screen infos
let width = window.innerWidth,
	height = window.innerHeight,
	isMobile = width < 768,
	ratioScreen = width / height,
	mapWidth = 1440,
	mapHeight = 900,
	ratioMap = mapWidth/mapHeight,
	svgHeight = (isMobile ? width :  height),
	svgWidth = (isMobile ? width :  height*ratioMap);

let ANIM_SPREAD_TIME = isMobile ? 2 * 1000 : 5 * 1000; // 2 seconds

if(ratioScreen < ratioMap && !isMobile){
	svgWidth = width;
	svgHeight = svgWidth/ratioMap;
	document.querySelector(".mapContainer").classList.add("horizontal");
}

let numberQatarPoints = 0;
let simulation, simulationCluster, counterAnimQatar,
counterAnimMigrant,
interval = [],
nodes = [];
let node,
intervalID,
countries,
gCircle,
positionOfQatarOnScreen;

function init() {

	d3.xml('./overmap.svg')
    .then(dataSVG => {
        d3.select(".map").html(dataSVG.documentElement.outerHTML);

        let svgMap = d3.select("svg")
        		.attr("width", svgWidth)
        		.attr("height", svgHeight);

        if (isMobile)
        	svgMap.attr("viewBox", null);


        let	qatar = svgMap.select("#qatar"),
        	legendQatar = document.getElementById("legendQatar"),
        	legendMigrant = document.getElementById("legendMigrant");

        gCircle = svgMap.append("g");
    	
    	positionOfQatarOnScreen = {
    		x: parseInt(isMobile ? svgWidth/2 : qatar.select(".countryMark").attr("x")),
    		y: parseInt(isMobile ? svgWidth/2 : qatar.select(".countryMark").attr("y"))
    	};

    	countries = Object.values(data);

    	simulationCluster = d3.forceSimulation(nodes.slice(0,numberQatarPoints))
    		.force('center', d3.forceCenter(positionOfQatarOnScreen.x, positionOfQatarOnScreen.y))
    		.force('cluster', cluster()
    			.strength(0.1))
    		.alphaDecay(0);
    	simulation = d3.forceSimulation(nodes)
        	.force("charge", d3.forceManyBody().strength(-3))
        	// .force('center', d3.forceCenter(width/2, height/2))
    		.force("x", d3.forceX(positionOfQatarOnScreen.x).strength(0.05))
    		.force("y", d3.forceY(positionOfQatarOnScreen.y).strength(0.05))
    		.alphaDecay(0);

		for (let i = 0; i < countries.length; i++) {
			let country = countries[i];
			let isQatar = country.name == "Qatar";
			let countryElement = svgMap.select("#" + country.id);

			if(isQatar){
				country.posX = width/2;
				country.posY = width/2;
			}
			
			//init pos of countries
			if(!isMobile){
				country.posX = parseInt(countryElement.select(".countryMark").attr("x"));
				country.posY = parseInt(countryElement.select(".countryMark").attr("y"));
			}


			country.displayedBoids = 0;

		}

		launchAnim(0,"down");

		clearInterval(intervalID);
		intervalID = window.setInterval(function(){
			reHeatCluster();
		}, 100);
		simulation.on("tick", ticked);
	});
}


function launchAnim(stepNum, dir) {
	console.log("launchAnim");

	initYear(stepNum, dir);
}


function initYear(step, dir) {

	updateCounter(step, dir);

	let date = d3.select(".date");

	for (let i = 0; i < countries.length; i++) {
		let country = countries[i];
		let name = country.name;
		let presence = country.presence[step];
		let boidsForThisStep = Math.round(presence / PERSON_PER_BOID);
		let diffBoids = boidsForThisStep - country.displayedBoids;
		let timeIntervalBetweenTwoBoids = Math.abs(ANIM_SPREAD_TIME / diffBoids);
		let posX = country.posX;
		let posY = country.posY;

		clearInterval(interval[name]);
		date.text(YEARS[step]);

		if(diffBoids){

			if(step == 0 || dir == "up"){
				updatePoint(country, positionOfQatarOnScreen, diffBoids);
			} else {
				interval[name] = setInterval(function(){
					updatePoint(country, {"x": posX, "y": posY}, (diffBoids < 0 ? -1 : 1) );
					if(country.displayedBoids >= boidsForThisStep){
						clearInterval(interval[name]);
					}
				}, timeIntervalBetweenTwoBoids);
			}
		}
	}
}

function updatePoint (country, coord, diffBoids) {
	let name = country.name;
	let isQatar = name == "Qatar";
	if(diffBoids > 0){
		addPoint({
			fill: (isQatar ? country.color : "#87CEFA"),
			country: name,
			x: isQatar || !isMobile ? coord.x : 0,
			y: isQatar || !isMobile ? coord.y : 0
		},diffBoids);
	} else {
		removePoint(name, Math.abs(diffBoids));
	}
	country.displayedBoids = document.querySelectorAll("[data-country='"+name+"']").length;
}

function addPoint(features,nbr = 1) {

	let isQatar = features.country == "Qatar";
	var addedPoints = d3.range(nbr).map(function(i) {
			return {
				fill: features.fill,
				radius: 3,
				country: features.country,
				x: (isQatar || !isMobile ? features.x + Math.random()*10-5 : randomPos().x), //random to avoid graphical bug
				y: (isQatar || !isMobile ? features.y + Math.random()*10-5 : randomPos().y)
			};
		});

	if(isQatar){
		nodes = addedPoints.concat(nodes);
	} else {
		nodes = nodes.concat(addedPoints);
	}


	simulation.nodes(nodes);
	restart();
}

function removePoint(country,nbr = 1) {
	let removedAmount = 0;
	for (let i = nodes.length - 1; i >= 0; i--) {
		if(nodes[i].country == country){
			nodes.splice(i, 1);
			removedAmount++;
			if(removedAmount == nbr ){
				break;
			}
		}
	}
	restart();
	simulation.nodes(nodes);
}

function ticked() {
	node.attr("cx", function(d) { return d.x; })
		.attr("cy", function(d) { return d.y; });
}

function restart() {
	
	node = gCircle.selectAll("circle")
			.data(nodes)
			.join("circle")
			.attr("r", 3)
			.attr("cx", (d) => d.x)
			.attr("cy", (d) => d.y)
			.attr("data-country", (d) => d.country)
			.attr("fill", (d) => d.fill);

	numberQatarPoints = document.querySelectorAll("[data-country='Qatar']").length;
}

function reHeatCluster() {
	simulationCluster = d3.forceSimulation(nodes.slice(0,numberQatarPoints))
		.force('center', d3.forceCenter(positionOfQatarOnScreen.x, positionOfQatarOnScreen.y))
		.force('cluster', cluster()
		.strength(0.005));
}


function cluster () {

	var nodes,
		strength = 0.1;

	function force (alpha) {

		// scale + curve alpha value
		alpha *= strength * alpha;
		nodes.slice(0, numberQatarPoints).forEach(function(d, i) {
			var cluster = nodes[0];
			if (cluster === d) return;

			let x = d.x - cluster.x,
			y = d.y - cluster.y,
			l = Math.sqrt(x * x + y * y),
			r = d.radius + cluster.radius;

			if (l != r) {
				l = (l - r) / l * alpha;
				d.x -= x *= l;
				d.y -= y *= l;
				cluster.x += x;
				cluster.y += y;
			}
		});
	}

	force.initialize = function (_) {
		nodes = _;
	}

	force.strength = _ => {
		strength = _ == null ? strength : _;
		return force;
	};
	return force;
}



function updateCounter(step, dir) {
	let yearQatarAmount = data.qatar.presence[step];
	let yearMigrantAmount = countries.reduce( ( sum, { presence } ) => sum + presence[step], 0) - yearQatarAmount;


	for (var i = 0; i < countries.length; i++) {
		let yearCountryAmount = countries[i].presence[step];
		let counterCountry;
		let textSlot = document.querySelector("#"+countries[i].id+" .countryNbr");

		if(textSlot) {
			changeCounter(step, dir, counterCountry, textSlot, yearCountryAmount);
		}
	}

	changeCounter(step, dir, counterAnimQatar, legendQatar, yearQatarAmount);
	changeCounter(step, dir, counterAnimMigrant, legendMigrant, yearMigrantAmount);

}

function changeCounter(step, dir, counter, textSlot, yearCountryAmount) {

	if(step == 0 || dir == "up"){
		if(counter){
			counter.reset();
		}
		textSlot.textContent = new Intl.NumberFormat('fr-FR').format(yearCountryAmount);
		textSlot.setAttribute("data-currentVal", yearCountryAmount);
	} else {
		counter = new CountUp(
			ANIM_SPREAD_TIME, 
			yearCountryAmount, 
			textSlot
		);
		counter.start();
	}	

}

function randomPos(){
	let isXfixed = Math.round(Math.random()*10)%2;
	let isYfixed = !isXfixed;

	let x = 0;
	let y = 0;

	if(isXfixed){
		x = Math.round(Math.random()*10)%2*width;
		y = Math.random()*10*width;
	} else {
		x = Math.random()*10*width;
		y = Math.round(Math.random()*10)%2*width;
	}
	return {"x":x, "y":y };
}



init();