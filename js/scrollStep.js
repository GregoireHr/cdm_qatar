// using d3 for convenience
var main = d3.select('#n-e')
var scrolly = main.select('#scrolly');
var figure = scrolly.select('.mapContainer');
var article = scrolly.select('.mapTextOverlay');
var step = article.selectAll('.step');

// initialize the scrollama
var scroller = scrollama();

// generic window resize listener event
function handleResize() {


	var figureHeight = window.innerHeight
	var figureMarginTop = (window.innerHeight - figureHeight) / 2  

	console.log(figureMarginTop);

	figure
		.style('height', figureHeight + 'px')
		.style('top', figureMarginTop + 'px');


	// 3. tell scrollama to update new element dimensions
	scroller.resize();
}

// scrollama event handlers
function handleStepEnter(response) {
	// response = { element, direction, index }

	// add color to current step only
	step.classed('is-active', function (d, i) {
		return i === response.index;
	});


	scrolly.attr("class","step-"+response.index);

	// update graphic based on step
	figure.select('p').text(response.index + 1);

	launchAnim(response.index, response.direction);
	if (response.index <= 2 && response.direction == "up") {
		simulation.restart();
		clearInterval(intervalID);
		intervalID = window.setInterval(function(){
			reHeatCluster();
		}, 100);
	}
}

function handleStepExit(response) {
	if (response.index == 2 && response.direction == "down") {
		simulation.stop();
		clearInterval(intervalID);
		console.log("clearInterval");
	}
}


function setupStickyfill() {
	d3.selectAll('.sticky').each(function () {
		Stickyfill.add(this);
	});
}

function init() {
	setupStickyfill();

	// 1. force a resize on load to ensure proper dimensions are sent to scrollama
	handleResize();

	// 2. setup the scroller passing options
	// 		this will also initialize trigger observations
	// 3. bind scrollama event handlers (this can be chained like below)
	scroller.setup({
		step: '#scrolly .mapTextOverlay .step',
		offset: 0.1,
		debug: false,
	})
		.onStepEnter(handleStepEnter)
		.onStepExit(handleStepExit)


	// setup resize event
	window.addEventListener('resize', handleResize);
}

// kick things off
init();