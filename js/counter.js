var CountUp = /** @class */ (function () {
	function CountUp(time, target, tagElmt){
		var _this = this;
		this.target = target;
		this.tagElmt = tagElmt;
		this.duration = time;
		this.startVal = 0;
		this.paused = true;
		this.currentVal = parseInt(tagElmt.getAttribute("data-currentVal"));
		this.diff = target - _this.currentVal;
		this.count = function (timestamp) {
		    if (!_this.startTime) {
		        _this.startTime = Date.now();
		    }
		    var progress = Date.now() - _this.startTime;

	    	let advancedNumber = progress/_this.duration * (_this.target - _this.currentVal) + _this.currentVal;
	    	if(!_this.paused){
	    		let numberValue = Math[(_this.diff < 0 ? "max" : "min")]( Math.trunc(advancedNumber), target);
	    		tagElmt.textContent = new Intl.NumberFormat('fr-FR').format(numberValue);
	    		tagElmt.setAttribute("data-currentVal", Math.trunc(numberValue));
	    	}

		    if (progress < _this.duration) {
		    	requestAnimationFrame(_this.count);
		    }
		}
	}

	// start animation
	CountUp.prototype.start = function (callback) {
		this.startTime = null;
		this.paused = false;
        this.rAF = requestAnimationFrame(this.count);
	};

	CountUp.prototype.reset = function () {
	    cancelAnimationFrame(this.rAF);
	    this.resetDuration();
	    this.resetDuration();
	};

	CountUp.prototype.resetDuration = function () {
		this.duration = 1;
	    this.startTime = null;
	    this.paused = true;
	    this.remaining = this.duration;
	};

	return CountUp;

}());